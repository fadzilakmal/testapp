(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tests-test-page-test-page-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tests/test-page/test-page.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tests/test-page/test-page.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Test 1</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <p>Please ensure that your details are correct.</p>\r\n  <p>{{ nric }}</p>\r\n  <p>{{ name }}</p>\r\n\r\n  <ion-button>Start Test</ion-button>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/tests/test-page/test-page-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/tests/test-page/test-page-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: TestPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestPagePageRoutingModule", function() { return TestPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _test_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./test-page.page */ "./src/app/pages/tests/test-page/test-page.page.ts");




const routes = [
    {
        path: '',
        component: _test_page_page__WEBPACK_IMPORTED_MODULE_3__["TestPagePage"]
    }
];
let TestPagePageRoutingModule = class TestPagePageRoutingModule {
};
TestPagePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TestPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tests/test-page/test-page.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/tests/test-page/test-page.module.ts ***!
  \***********************************************************/
/*! exports provided: TestPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestPagePageModule", function() { return TestPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./test-page-routing.module */ "./src/app/pages/tests/test-page/test-page-routing.module.ts");
/* harmony import */ var _test_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./test-page.page */ "./src/app/pages/tests/test-page/test-page.page.ts");







let TestPagePageModule = class TestPagePageModule {
};
TestPagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _test_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["TestPagePageRoutingModule"]
        ],
        declarations: [_test_page_page__WEBPACK_IMPORTED_MODULE_6__["TestPagePage"]]
    })
], TestPagePageModule);



/***/ }),

/***/ "./src/app/pages/tests/test-page/test-page.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/tests/test-page/test-page.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Rlc3RzL3Rlc3QtcGFnZS90ZXN0LXBhZ2UucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/tests/test-page/test-page.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/tests/test-page/test-page.page.ts ***!
  \*********************************************************/
/*! exports provided: TestPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestPagePage", function() { return TestPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");




let TestPagePage = class TestPagePage {
    constructor(auth, router) {
        this.auth = auth;
        this.router = router;
        this.name = '';
        this.nric = '';
    }
    ngOnInit() {
        this.name = this.auth.name;
        this.nric = this.auth.nric;
    }
};
TestPagePage.ctorParameters = () => [
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
TestPagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test-page',
        template: __webpack_require__(/*! raw-loader!./test-page.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tests/test-page/test-page.page.html"),
        styles: [__webpack_require__(/*! ./test-page.page.scss */ "./src/app/pages/tests/test-page/test-page.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], TestPagePage);



/***/ })

}]);
//# sourceMappingURL=pages-tests-test-page-test-page-module-es2015.js.map