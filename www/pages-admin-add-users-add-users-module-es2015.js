(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-admin-add-users-add-users-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/admin/add-users/add-users.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/admin/add-users/add-users.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>addUsers</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/admin/add-users/add-users-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/admin/add-users/add-users-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: AddUsersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUsersPageRoutingModule", function() { return AddUsersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_users_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-users.page */ "./src/app/pages/admin/add-users/add-users.page.ts");




const routes = [
    {
        path: '',
        component: _add_users_page__WEBPACK_IMPORTED_MODULE_3__["AddUsersPage"]
    }
];
let AddUsersPageRoutingModule = class AddUsersPageRoutingModule {
};
AddUsersPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddUsersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/admin/add-users/add-users.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/admin/add-users/add-users.module.ts ***!
  \***********************************************************/
/*! exports provided: AddUsersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUsersPageModule", function() { return AddUsersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_users_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-users-routing.module */ "./src/app/pages/admin/add-users/add-users-routing.module.ts");
/* harmony import */ var _add_users_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-users.page */ "./src/app/pages/admin/add-users/add-users.page.ts");







let AddUsersPageModule = class AddUsersPageModule {
};
AddUsersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _add_users_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddUsersPageRoutingModule"]
        ],
        declarations: [_add_users_page__WEBPACK_IMPORTED_MODULE_6__["AddUsersPage"]]
    })
], AddUsersPageModule);



/***/ }),

/***/ "./src/app/pages/admin/add-users/add-users.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/admin/add-users/add-users.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2FkZC11c2Vycy9hZGQtdXNlcnMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/admin/add-users/add-users.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/admin/add-users/add-users.page.ts ***!
  \*********************************************************/
/*! exports provided: AddUsersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUsersPage", function() { return AddUsersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AddUsersPage = class AddUsersPage {
    constructor() { }
    ngOnInit() {
    }
};
AddUsersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-users',
        template: __webpack_require__(/*! raw-loader!./add-users.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/admin/add-users/add-users.page.html"),
        styles: [__webpack_require__(/*! ./add-users.page.scss */ "./src/app/pages/admin/add-users/add-users.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AddUsersPage);



/***/ })

}]);
//# sourceMappingURL=pages-admin-add-users-add-users-module-es2015.js.map