import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  name = '';
  uid = '';

  constructor(
    private auth: AuthService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.name = this.auth.name;
    this.uid = this.auth.userID;
  }

}
