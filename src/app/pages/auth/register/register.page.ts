import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  result: any;
  
  constructor(
    private fb: FormBuilder, 
    private auth: AuthService, 
    private alertCtrl: AlertController, 
    private toastCtrl: ToastController, 
    private router: Router
  ) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      nric: ['', [Validators.required]],
      password: ['test123', [Validators.required, Validators.minLength(6)]],
      name: ['', Validators.required],
      role: ['USER', Validators.required]
    });
  }

  registerUser() {
    this.auth.isUserAvailable(this.registerForm.value.nric).subscribe(res => {
      if (res.length > 0) {
        let alert = this.alertCtrl.create({
          header: 'Error',
          message: 'NRIC already registered',
          buttons: ['OK']
        });
        alert.then(alert => alert.present());
      } else {
        this.auth.signUp(this.registerForm.value).then(async (res) => {
        
          let toast = await this.toastCtrl.create({
            duration: 3000,
            message: 'Successfully created new Account!'
          });
          toast.present();
          this.navigateByRole(this.registerForm.value['role']);
        }, async (err) => {
          let alert = await this.alertCtrl.create({
            header: 'Error',
            message: err.message,
            buttons: ['OK']
          });
          alert.present();
        })
      }
    })
  }

  navigateByRole(role) {
    if (role == 'USER') {
      this.router.navigateByUrl('/dashboard');
    } else if (role == 'TESTER') {
      this.router.navigateByUrl('/dashboard');
    }
  }

}
