import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController, ToastController, LoadingController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  loginError: string;

  constructor(
    private form: FormBuilder, 
    private auth: AuthService, 
    private alertCtrl: AlertController, 
    private toastCtrl: ToastController, 
    private router: Router,
    private loadingCtrl: LoadingController,
    private menuCtrl: MenuController,
    
  ) {  

    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.loginForm = this.form.group({
      nric: ['', [Validators.required]],
      password: ['test123', [Validators.required, Validators.minLength(6)]]
    });
  }

  ionViewWillEnter() {
     this.menuCtrl.enable(false);
  }

 login() {
    this.auth.signIn(this.loginForm.value).subscribe(user => {
      console.log('after login: ', user);
      this.navigateByRole(user['role']);
    }, async (err) => {
      let alert = await this.alertCtrl.create({
        header: 'Error',
        message: err.message,
        buttons: ['OK']
      });
      alert.present();
    })
  }

  async openReset() {
    let inputAlert = await this.alertCtrl.create({
      header: 'Reset Password',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Reset',
          handler: data => {
            this.resetPw(data.email);
          }
        }
      ]
    });
    inputAlert.present();
  }

  navigateByRole(role) {
    if (role == 'USER') {
      this.router.navigateByUrl('/dashboard');
    } else if (role == 'TESTER') {
      this.router.navigateByUrl('/dashboard');
    } else if (role == 'ADMIN') {
      this.router.navigateByUrl('/admin-dashboard');
    }
  }

  resetPw(email) {
    this.auth.resetPw(email).then(async (res) => {
      let toast = await this.toastCtrl.create({
        duration: 3000,
        message: 'Success! Check your Emails for more information.'
      });
      toast.present();
    }, async (err) => {
      let alert = await this.alertCtrl.create({
        header: 'Error',
        message: err.message,
        buttons: ['OK']
      });
      alert.present();
    });
  }
 
}
