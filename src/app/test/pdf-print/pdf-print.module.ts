import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PdfPrintPageRoutingModule } from './pdf-print-routing.module';

import { PdfPrintPage } from './pdf-print.page';
import { HttpClient } from '@angular/common/http';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdfPrintPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [PdfPrintPage]
})
export class PdfPrintPageModule {}
