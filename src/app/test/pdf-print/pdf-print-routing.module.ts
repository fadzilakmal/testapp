import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PdfPrintPage } from './pdf-print.page';

const routes: Routes = [
  {
    path: '',
    component: PdfPrintPage
  }
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PdfPrintPageRoutingModule {}
