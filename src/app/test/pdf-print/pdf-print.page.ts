import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-pdf-print',
  templateUrl: './pdf-print.page.html',
  styleUrls: ['./pdf-print.page.scss'],
})
export class PdfPrintPage implements OnInit {

  pdfObj = null;

  constructor(
 
  ) { }

  ngOnInit() {
  }

  pdfDownload() {
    const docDef = {
      pageSize: 'A4',

      pageOrientation: 'portrait',

      pageMargins: [20,10,40,60],
      content: [
        "your mom green"
      ]
    }

    this.pdfObj = pdfMake.createPdf(docDef);
    this.pdfObj.download('demo.pdf');
    console.log('PDF downloaded')
  }

 

  
}
