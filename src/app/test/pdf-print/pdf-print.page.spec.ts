import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PdfPrintPage } from './pdf-print.page';

describe('PdfPrintPage', () => {
  let component: PdfPrintPage;
  let fixture: ComponentFixture<PdfPrintPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfPrintPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PdfPrintPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  })); 

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
